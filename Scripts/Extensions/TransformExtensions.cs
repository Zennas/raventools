﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    /// <summary>
    /// Sets X value of a transform position
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newX"></param>
    /// <param name="localSpace"></param>
    public static void SetPostionX(this Transform t, float newX, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentPosition = t.position;
            currentPosition.x = newX;
            t.position = currentPosition;
        }
        else
        {
            Vector3 currentPosition = t.localPosition;
            currentPosition.x = newX;
            t.localPosition = currentPosition;
        }
    }

    /// <summary>
    /// Sets Y value of a transform position
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newY"></param>
    /// <param name="localSpace"></param>
    public static void SetPostionY(this Transform t, float newY, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentPosition = t.position;
            currentPosition.y = newY;
            t.position = currentPosition;
        }
        else
        {
            Vector3 currentPosition = t.localPosition;
            currentPosition.y = newY;
            t.localPosition = currentPosition;
        }
    }

    /// <summary>
    /// Sets Z value of a transform position
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newZ"></param>
    /// <param name="localSpace"></param>
    public static void SetPostionZ(this Transform t, float newZ, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentPosition = t.position;
            currentPosition.z = newZ;
            t.position = currentPosition;
        }
        else
        {
            Vector3 currentPosition = t.localPosition;
            currentPosition.z = newZ;
            t.localPosition = currentPosition;
        }
    }

    /// <summary>
    /// Sets X value of a transform rotation
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newX"></param>
    /// <param name="localSpace"></param>
    public static void SetRotationX(this Transform t, float newX, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentRotation = t.eulerAngles;
            currentRotation.x = newX;
            t.rotation = Quaternion.Euler(currentRotation);
        }
        else
        {
            Vector3 currentRotation = t.localEulerAngles;
            currentRotation.x = newX;
            t.localRotation = Quaternion.Euler(currentRotation);
        }
    }

    /// <summary>
    /// Sets Y value of a transform rotation
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newY"></param>
    /// <param name="localSpace"></param>
    public static void SetRotationY(this Transform t, float newY, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentRotation = t.eulerAngles;
            currentRotation.y = newY;
            t.rotation = Quaternion.Euler(currentRotation);
        }
        else
        {
            Vector3 currentRotation = t.localEulerAngles;
            currentRotation.y = newY;
            t.localRotation = Quaternion.Euler(currentRotation);
        }
    }

    /// <summary>
    /// Sets Z value of a transform rotation
    /// </summary>
    /// <param name="t"></param>
    /// <param name="newZ"></param>
    /// <param name="localSpace"></param>
    public static void SetRotationZ(this Transform t, float newZ, bool localSpace = false)
    {
        if (!localSpace)
        {
            Vector3 currentRotation = t.eulerAngles;
            currentRotation.z = newZ;
            t.rotation = Quaternion.Euler(currentRotation);
        }
        else
        {
            Vector3 currentRotation = t.localEulerAngles;
            currentRotation.z = newZ;
            t.localRotation = Quaternion.Euler(currentRotation);
        }
    }

    /// <summary>
    /// Sets X value of a transform scale
    /// </summary>
    /// <param name="t"></param>
    /// <param name="x"></param>
    public static void SetScaleX(this Transform t, float x)
    {
        Vector3 newScale = new Vector3(x, t.localScale.y, t.localScale.z);
        t.localScale = newScale;
    }

    /// <summary>
    /// Sets Y value of a transform scale
    /// </summary>
    /// <param name="t"></param>
    /// <param name="y"></param>
    public static void SetScaleY(this Transform t, float y)
    {
        Vector3 newScale = new Vector3(t.localScale.x, y, t.localScale.z);
        t.localScale = newScale;
    }

    /// <summary>
    /// Sets Z value of a transform scale
    /// </summary>
    /// <param name="t"></param>
    /// <param name="z"></param>
    public static void SetScaleZ(this Transform t, float z)
    {
        Vector3 newScale = new Vector3(t.localScale.x, t.localScale.y, z);
        t.localScale = newScale;
    }

    /// <summary>
    /// Sets all scale axiss to the given value
    /// </summary>
    /// <param name="t"></param>
    /// <param name="scale"></param>
    public static void SetEvenScale(this Transform t, float scale)
    {
        Vector3 newScale = new Vector3(scale, scale, scale);
        t.localScale = newScale;
    }

    /// <summary>
    /// Shortcut for t.parent = null
    /// </summary>
    /// <param name="t"></param>
    public static void ClearParent(this Transform t)
    {
        t.parent = null;
    }

    /// <summary>
    /// Returns the top parent in an object transform hierarchy
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public static Transform GetRootParent(this Transform t)
    {
        Transform p = t;

        while (p.parent != null)
        {
            p = p.parent;
        }

        return p;
    }
    
    /// <summary>
    /// Alings transforms rotation to look at a target without affecting the X and Z rotation
    /// </summary>
    /// <param name="t"></param>
    /// <param name="target"></param>
    /// <param name="local"></param>
    public static void LockAtTargetYAxiss(this Transform t, Vector3 target, bool local = false)
    {
        t.LookAt(target);

        Vector3 currentRotation = local ? t.localRotation.eulerAngles : t.rotation.eulerAngles;
        currentRotation.x = 0;
        currentRotation.z = 0;

        if (local)
            t.localRotation = Quaternion.Euler(currentRotation);
        else
            t.rotation = Quaternion.Euler(currentRotation);
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Tools/RavenTools/PlaceOnFloor")]
    public static void PlaceOnFloor()
    {
        var selections = UnityEditor.Selection.gameObjects;

        for (int i = 0; i < selections.Length; i++)
        {
            Ray ray = new Ray(selections[i].transform.position, Vector3.down);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                selections[i].transform.position = hit.point;
            }

        }
    }

    [UnityEditor.MenuItem("Tools/RavenTools/RandomizeRotationY")]
    public static void RandomizeRotationY()
    {
        var selections = UnityEditor.Selection.gameObjects;

        for (int i = 0; i < selections.Length; i++)
        {
            selections[i].transform.localRotation = Quaternion.Euler(0, Random.Range(0,360), 0);
        }
    }
#endif
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AssetBundleExtensions
{
    public static void UnloadBundle(string name, bool unloadAllObjects = true)
    {
        var bundles = AssetBundle.GetAllLoadedAssetBundles();
        foreach (var bundle in bundles)
        {
            if (bundle.name.Equals(name))
            {
                bundle.Unload(unloadAllObjects);
                break;
            }
        }
    }

    public static bool IsBundleLoaded(string name)
    {
        var bundles = AssetBundle.GetAllLoadedAssetBundles();
        foreach (var bundle in bundles)
        {
            if (bundle.name.Equals(name))
            {
                return true;
            }
        }

        return false;
    }
}

﻿using System.Text;
public static class ByteExtensions
{
    public static string BytesToString(this byte[] bytes)
    {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < bytes.Length; i++)
        {
            stringBuilder.Append(bytes[i].ToString("X"));
        }

        return stringBuilder.ToString();
    }
}

﻿using UnityEngine;

public static class InternetExtensions
{
    /// <summary>
    /// Shortcut for Application.internetReachability != NetworkReachability.NotReachable
    /// </summary>
    public static bool HasInternetAccess
    {
        get
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }
    }
}
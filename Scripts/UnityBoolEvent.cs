﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace raventools
{
    [System.Serializable]
    public class UnityBoolEvent : UnityEvent<bool>
    {
    }
}
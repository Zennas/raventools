﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class FullScreenRectTransform : MonoBehaviour
{
    private RectTransform rect;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    public void SetHeight(float newHeight)
    {
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
    }
}
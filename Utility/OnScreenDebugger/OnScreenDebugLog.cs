﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenDebugLog : MonoBehaviour
{
    private static OnScreenDebugLog instance;

    [SerializeField] private RectTransform scrollView;
    [SerializeField] private RectTransform container;

    [SerializeField, Range(1, 200)]
    private int maxLogCount = 10;

    [SerializeField]
    private UnityEngine.UI.Text display;

    [SerializeField]
    private Color[] logColors = new Color[3] { Color.red, Color.yellow, Color.black };

    [Header("Buttons")]
    [SerializeField] private UnityEngine.UI.Button fullscreenButton;
    [SerializeField] private UnityEngine.UI.Button infoButton;
    [SerializeField] private UnityEngine.UI.Button warningButton;
    [SerializeField] private UnityEngine.UI.Button errorButton;

    string myLog;
    private Queue<UnityEngine.UI.Text> myLogQueue = new Queue<UnityEngine.UI.Text>();
    private List<UnityEngine.UI.Text> activeLogTexts = new List<UnityEngine.UI.Text>();

    private bool showInfo = true;
    private bool showWarning = true;
    private bool showError = true;

    private bool isFullScreen;
    private RectTransform rectTransform;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

#if UNITY_EDITOR
        gameObject.SetActive(false);
#endif

#if RT_RELEASE || RT_QA
        Destroy(gameObject);
#endif
    }

    void Start()
    { 
        DontDestroyOnLoad(gameObject);
        rectTransform = GetComponent<RectTransform>();

        errorButton.image.color = new Color(logColors[0].r, logColors[0].g, logColors[0].b, .5f);
        warningButton.image.color = new Color(logColors[1].r, logColors[1].g, logColors[1].b, .5f);
        infoButton.image.color = new Color(Color.white.r, Color.white.g, Color.white.b, .5f);
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        myLog = logString;

        if(myLog.Length >= 3500)
        {
            myLog = myLog.Substring(0, 3500) + "...";
        }

        string newString = $"\n{System.DateTime.Now.ToShortTimeString()} [{type}] : {myLog}";

        UnityEngine.UI.Text tmpText;

        if (myLogQueue.Count <= maxLogCount)
        {
            tmpText = (UnityEngine.UI.Text)Instantiate(display, container);
            tmpText.text = newString.Trim();
            myLogQueue.Enqueue(tmpText);
            activeLogTexts.Add(tmpText);

            switch (type)
            {
                case LogType.Error:
                    tmpText.color = logColors[0];
                    break;
                case LogType.Assert:
                    tmpText.color = logColors[2];
                    break;
                case LogType.Warning:
                    tmpText.color = logColors[1];
                    break;
                case LogType.Log:
                    tmpText.color = logColors[2];
                    break;
                case LogType.Exception:
                    tmpText.color = logColors[0];
                    break;
            }
        }
        else
        {
            tmpText = myLogQueue.Dequeue();
            tmpText.text = newString.Trim();
            tmpText.rectTransform.SetAsLastSibling();

            myLogQueue.Enqueue(tmpText);

            switch (type)
            {
                case LogType.Error:
                    tmpText.color = logColors[0];
                    break;
                case LogType.Assert:
                    tmpText.color = logColors[2];
                    break;
                case LogType.Warning:
                    tmpText.color = logColors[1];
                    break;
                case LogType.Log:
                    tmpText.color = logColors[2];
                    break;
                case LogType.Exception:
                    tmpText.color = logColors[0];
                    break;
            }
        }

        if (tmpText.color == logColors[0] && !showError)
            tmpText.gameObject.SetActive(false);


        if (tmpText.color == logColors[1] && !showWarning)
            tmpText.gameObject.SetActive(false);


        if (tmpText.color == logColors[2] && !showInfo)
            tmpText.gameObject.SetActive(false);

        container.ForceUpdateRectTransforms();
    }

    public void ClearLog()
    {
        for (int i = 0; i < container.childCount; i++)
        {
            container.GetChild(i).GetComponent<UnityEngine.UI.Text>().text = string.Empty;
        }
    }
    
    public void ToogleInformation()
    {
        showInfo = !showInfo;

        for (int i = 0; i < activeLogTexts.Count; i++)
        {
            if(activeLogTexts[i].color == logColors[2])
            {
                activeLogTexts[i].gameObject.SetActive(showInfo);
            }
        }

        Color c = new Color(Color.white.r, Color.white.g, Color.white.b, .5f);

        if (!showInfo)
        {
            c = new Color(c.r / 2, c.g / 2, c.b / 2, .5f);
        }

        infoButton.image.color = c;
    }

    public void ToogleWarings()
    {
        showWarning = !showWarning;

        for (int i = 0; i < activeLogTexts.Count; i++)
        {
            if (activeLogTexts[i].color == logColors[1])
            {
                activeLogTexts[i].gameObject.SetActive(showWarning);
            }
        }

        Color c = new Color(logColors[1].r, logColors[1].g, logColors[1].b, .5f);

        if (!showWarning)
        {
            c = new Color(c.r / 2, c.g / 2, c.b / 2, c.a);
        }

        warningButton.image.color = c;
    }

    public void ToogleErrors()
    {
        showError = !showError;

        for (int i = 0; i < activeLogTexts.Count; i++)
        {
            if (activeLogTexts[i].color == logColors[0])
            {
                activeLogTexts[i].gameObject.SetActive(showError);
            }
        }

        Color c = new Color(logColors[0].r, logColors[0].g, logColors[0].b, .5f);

        if (!showError)
        {
            c = new Color(c.r / 2, c.g / 2, c.b / 2, c.a);
        }

        errorButton.image.color = c;
    }

    public void ToogleFullscreen()
    {
        isFullScreen = !isFullScreen;

        if (isFullScreen)
        {
            scrollView.sizeDelta = new Vector2(scrollView.sizeDelta.x, rectTransform.sizeDelta.y - 30);
            fullscreenButton.GetComponentInChildren<UnityEngine.UI.Text>().text = "Smallscreen";
        }
        else
        {
            scrollView.sizeDelta = new Vector2(scrollView.sizeDelta.x, rectTransform.sizeDelta.y * .2f);
            fullscreenButton.GetComponentInChildren<UnityEngine.UI.Text>().text = "Fullscreen";
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    public class LoadSceneOnBackPressed : MonoBehaviour
    {
        [SerializeField]
        private SceneController sceneController;
        [SerializeField]
        private int sceneToLoadIndex;
                
        protected void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                sceneController.LoadScene(sceneToLoadIndex);
            }
        }
    }
}
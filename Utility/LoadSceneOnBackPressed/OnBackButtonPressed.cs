﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture.utils
{
    public class OnBackButtonPressed : MonoBehaviour
    {
        public UnityEngine.Events.UnityEvent onBack;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (onBack != null)
                    onBack.Invoke();
            }
        }

    }
}
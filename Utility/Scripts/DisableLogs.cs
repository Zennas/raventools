﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableLogs : MonoBehaviour
{
#if UNITY_EDITOR
    public bool debugLogs;
#endif

    private void Start()
    {
#if !UNITY_EDITOR
        Debug.unityLogger.logEnabled = false;
#else
        Debug.unityLogger.logEnabled = debugLogs;
#endif
    }
}

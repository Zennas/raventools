﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutAtAwake : MonoBehaviour
{
    [SerializeField] private ScriptableArchitecture.FadeScreenController fader;

	protected void Awake ()
	{
        fader.FadeOut();	
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetFps : MonoBehaviour
{
    [SerializeField] private int frameRate = 60;

    private void Awake()
    {
        Application.targetFrameRate = frameRate;
    }
}

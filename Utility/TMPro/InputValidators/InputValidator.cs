﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace raventools.utils.inputvalidator
{
    public abstract class InputValidator : MonoBehaviour
    {
        [SerializeField]
        protected UnityBoolEvent OnValidate;
        protected bool isValid;

        protected string text;

        public abstract void ValidateInput(string textToValidate);

        public virtual string GetInput()
        {
            return text;
        }

        public abstract void SetInput(string value);

        public virtual bool IsValid()
        {
            return isValid;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raventools.utils.inputvalidator
{
    public class NotNullOrEmptyValidator : InputValidator
    {
        public override void SetInput(string value)
        {
            GetComponent<TMPro.TMP_InputField>().text = value;
            ValidateInput(value);
        }

        public override void ValidateInput(string textToValidate)
        {
            bool result = !string.IsNullOrWhiteSpace(textToValidate);
            isValid = result;
            OnValidate?.Invoke(result);

            text = isValid ? textToValidate : string.Empty;
        }
    }
}
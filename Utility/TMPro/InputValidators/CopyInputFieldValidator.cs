﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raventools.utils.inputvalidator
{
    public class CopyInputFieldValidator : InputValidator
    {
        [SerializeField] private TMPro.TMP_InputField compareInputField;

        public override void SetInput(string value)
        {
            return;
        }

        public override void ValidateInput(string textToValidate)
        {
            bool result;
            if (string.IsNullOrWhiteSpace(textToValidate))
                result = false;
            else
                result = textToValidate.Equals(compareInputField.text);

            OnValidate?.Invoke(result);
            isValid = result;

            text = isValid ? textToValidate : string.Empty;
        }

        public void ValidateInputIfCompareFieldWasEdited()
        {
            ValidateInput(GetInput());
        }
    }
}
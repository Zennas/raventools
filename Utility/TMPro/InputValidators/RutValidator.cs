﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Text.RegularExpressions;

namespace raventools.utils.inputvalidator
{
    public class RutValidator : InputValidator
    {
        public override void SetInput(string value)
        {
            GetComponent<TMPro.TMP_InputField>().text = value;
            ValidateInput(value);
        }

        public override void ValidateInput(string textToValidate)
        {
            bool result;
            if (string.IsNullOrEmpty(textToValidate))
                result = false;
            else
                result = ValidaRut(textToValidate);

            isValid = result;
            OnValidate?.Invoke(result);

            text = isValid ? textToValidate : string.Empty;
        }

        /// <summary>
        /// Metodo de validación de rut con digito verificador
        /// dentro de la cadena
        /// </summary>
        /// <param name="rut">string</param>
        /// <returns>booleano</returns>
        public static bool ValidaRut(string rut)
        {
            rut = rut.Replace(".", "").ToUpper();
            Regex expresion = new Regex("^([0-9]+-{0,1}[0-9K])$");
            if (!expresion.IsMatch(rut))
            {
                return false;
            }

            rut = rut.Replace("-", "").ToUpper();

            if (rut.Length < 8 || rut.Length > 9)
                return false;

            string dv = rut.Substring(rut.Length - 1, 1);
            string r = rut.Substring(0, rut.Length - 1);
            
            if (dv != Digito(int.Parse(r)))
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Método que valida el rut con el digito verificador
        /// por separado
        /// </summary>
        /// <param name="rut">integer</param>
        /// <param name="dv">char</param>
        /// <returns>booleano</returns>
        public static bool ValidaRut(string rut, string dv)
        {
            return ValidaRut(rut + "-" + dv);
        }

        /// <summary>
        /// método que calcula el digito verificador a partir
        /// de la mantisa del rut
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static string Digito(int rut)
        {
            int suma = 0;
            int multiplicador = 1;
            while (rut != 0)
            {
                multiplicador++;
                if (multiplicador == 8)
                    multiplicador = 2;
                suma += (rut % 10) * multiplicador;
                rut = rut / 10;
            }
            suma = 11 - (suma % 11);
            if (suma == 11)
            {
                return "0";
            }
            else if (suma == 10)
            {
                return "K";
            }
            else
            {
                return suma.ToString();
            }
        }
    }
}
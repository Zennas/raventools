﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    public class SimpleSceneTransition : MonoBehaviour
    {
        public SceneController sceneController;
        public FadeScreenController fadeController;

        private bool inTransition;

        public void TransitionToScene(string scene)
        {
            if (inTransition)
                return;

            inTransition = true;
            fadeController.FadeIn(() => { sceneController.LoadSceneAsync(scene); });
        }

        public void TransitionToScene(int sceneIndex)
        {
            if (inTransition)
                return;

            inTransition = true;
            fadeController.FadeIn(() => { sceneController.LoadSceneAsync(sceneIndex); });
        }
    }
}
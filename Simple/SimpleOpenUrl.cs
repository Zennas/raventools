﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raventools.simple
{
    public class SimpleOpenUrl : MonoBehaviour
    {
        public void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace raventools.simple
{
    public class SimpleTimer : MonoBehaviour
    {
        public float timer;
        private float _timer;

        public UnityEvent actions;

        private void OnEnable()
        {
            _timer = timer;
        }

        private void Update()
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                actions?.Invoke();
            }
        }
    }
}
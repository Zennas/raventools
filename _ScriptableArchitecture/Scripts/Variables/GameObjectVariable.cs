﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/GameObjectVariable")]
	public class GameObjectVariable : ScriptableVariable
	{
		#if UNITY_EDITOR
		[TextArea]
		public string DeveloperDescription = "";
		#endif
		
		public GameObject Value;

		public void SetValue(GameObject value)
		{
			Value = value;

			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}
	}
}
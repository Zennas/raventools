﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/FloatVariable")]
	public class FloatVariable : ScriptableVariable
	{
		#if UNITY_EDITOR
		[TextArea]
		public string DeveloperDescription = "";
		#endif
		public float Value;

		public void SetValue(float value)
		{
			Value = value;

			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}

		public void ApplyChange(float amount)
		{
			SetValue(Value + amount);
		}
	}
}

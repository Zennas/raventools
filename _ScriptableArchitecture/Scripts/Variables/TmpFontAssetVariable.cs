using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/TMP FontAssetVariable")]
    public class TmpFontAssetVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public TMPro.TMP_FontAsset Value;

        public void SetValue(TMPro.TMP_FontAsset value)
        {
            Value = value;

            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }
    }
}
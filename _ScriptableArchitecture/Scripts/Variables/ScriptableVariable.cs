using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    public class ScriptableVariable : ScriptableObject
    {
        [SerializeField] private GameEvent updateEvent;

        public GameEvent UpdateEvent
        {
            get
            {
                return updateEvent;
            }
        }

        //protected void OnValidate()
        //{
        //    if (updateEvent != null)
        //        updateEvent.Raise();
        //}
    }
}
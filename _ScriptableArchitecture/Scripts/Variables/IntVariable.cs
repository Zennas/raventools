// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/IntVariable")]
    public class IntVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif
        public int Value;

        public void SetValue(int value)
        {
            //Debug.Log("Changing value of " + name + " from " + Value + " to " + value);
            Value = value;

            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }

        public void ApplyChange(int amount)
        {
            SetValue(Value + amount);
        }
    }
}
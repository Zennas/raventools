﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/AudioClipVariable")]
    public class AudioClipVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public AudioClip Value;

        public void SetValue(AudioClip value)
        {
            Value = value;

            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }
    }
}
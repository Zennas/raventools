using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/SpriteVariable")]
	public class SpriteVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public Sprite Value;

		public void SetValue(Sprite value)
		{
			Value = value;

			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}
    }
}
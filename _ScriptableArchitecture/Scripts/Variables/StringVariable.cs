﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/StringVariable")]
    public class StringVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public string Value;

        public void SetValue(string value)
        {
            Value = value;

            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }
    }
}
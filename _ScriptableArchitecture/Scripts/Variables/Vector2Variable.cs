﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/Vector2Variable")]
    public class Vector2Variable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public Vector2 Value;

        public void SetValue(Vector2 value)
        {
            Value.Set(value.x, value.y);

            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/ColorVariable")]
	public class ColorVariable : ScriptableVariable
    {
    #if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
    #endif

        public Color Value;
        
		public void SetValue(Color value)
		{
			Value = value;
			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}
    }
}
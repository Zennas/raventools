﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/Texture2DVariable")]
    public class Texture2DVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public Texture2D Value;

        public void SetValue(Texture2D value)
        {
            Value = value;
            if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
        }
    }
}
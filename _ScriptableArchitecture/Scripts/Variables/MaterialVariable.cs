﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/MaterialVariable")]
	public class MaterialVariable : ScriptableVariable
	{
		#if UNITY_EDITOR
		[TextArea]
		public string DeveloperDescription = "";
		#endif
		
		public Material Value;

		public void SetValue(Material value)
		{
			Value = value;

			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}
	}
}
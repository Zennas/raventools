﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/Vector3Variable")]
	public class Vector3Variable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public Vector3 Value;

		public void SetValue(Vector3 value)
		{
			Value.Set(value.x, value.y, value.z);

			if (base.UpdateEvent != null)
            {
                base.UpdateEvent.Raise();
            }
		}
    }
}
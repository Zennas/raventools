using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/GradientVariable")]
	public class GradientVariable : ScriptableVariable
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        public Gradient Value;

		public void SetValue(Gradient value)
		{
			Value = value;

			if (base.UpdateEvent != null)
			{
				base.UpdateEvent.Raise();
			}
		}
    }
}

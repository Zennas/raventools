﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Variables/BoolVariable")]
	public class BoolVariable : ScriptableVariable
	{
	#if UNITY_EDITOR
		[TextArea]
	    public string DeveloperDescription = "";
	#endif
	
	    public bool Value;

        public void SetValue(bool value)
        {
            Value = value;
            base.UpdateEvent?.Raise();
        }
	}
}
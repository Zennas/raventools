using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Globalization;

namespace ScriptableArchitecture.utils
{
    [RequireComponent(typeof(GameEventListener))]
    public class UpdateTMPTextByIntVariable : MonoBehaviour
    {
        [SerializeField] private IntVariable valueToSet;
        [SerializeField] private TMPro.TextMeshProUGUI targetText;

        [Space]
        [SerializeField] private string culture = "es-CL";
        [SerializeField] private string format = "N0";

        private void OnEnable()
        {
            UpdateText();
        }

        public void UpdateText()
        {
            NumberFormatInfo nfi = new CultureInfo(culture, false).NumberFormat;
            targetText.text = valueToSet.Value.ToString(format, nfi);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture.utils
{
    public class SetGameObjectAtAwake : MonoBehaviour
    {
        [SerializeField] private ScriptableArchitecture.GameObjectVariable gameobjectContainer;

        private void Awake()
        {
            gameobjectContainer.Value = gameObject;
        }
    }
}
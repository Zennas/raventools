﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture.utils
{
    public class SetTMProByStringVariable : MonoBehaviour
    {
        [SerializeField] private StringVariable variable;

        private void Start()
        {
            GetComponent<TMPro.TextMeshProUGUI>().text = variable.Value;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture.utils
{
    public class RaiseEventAtStart : MonoBehaviour
    {
        [SerializeField] private GameEvent eventToRaise;
        void Start()
        {
            eventToRaise?.Raise();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture.utils
{
    public class DestroyGameObject : MonoBehaviour
    {
        public void Destroy(GameObject target)
        {
            Destroy(target);
        }

        public void DestroySelf()
        {
            Destroy(gameObject);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    public class SetComponentStateByBoolVariable : MonoBehaviour
    {
        [SerializeField] private Behaviour target;
        [SerializeField] private BoolVariable observedVariable;
        
        public void SetComponentState()
        {
            target.enabled = observedVariable.Value;
        }
    }
}
﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Events/GameEvent")]
	public class GameEvent : ScriptableObject
	{
#if UNITY_EDITOR
        public bool logRaise = false;
        [TextArea]
        public string DeveloperDescription = "";

        public List<string> RegisteredGameObjects = new List<string>();
#endif

        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GameEventListener> eventListeners = 
			new List<GameEventListener>();

		public void Raise()
		{
#if UNITY_EDITOR
            if(logRaise)
                Debug.Log("EventRaised : " + this.name);
#endif
			for(int i = eventListeners.Count -1; i >= 0; i--)
				eventListeners[i].OnEventRaised();
		}

		public void RegisterListener(GameEventListener listener)
		{
            if (!eventListeners.Contains(listener))
            {
                eventListeners.Add(listener);
#if UNITY_EDITOR
                UpdateRegisteredGameObjectList();
#endif
            }
        }

		public void UnregisterListener(GameEventListener listener)
		{
            if (eventListeners.Contains(listener))
            {
                eventListeners.Remove(listener);
#if UNITY_EDITOR
                UpdateRegisteredGameObjectList();
#endif
        }
    }

#if UNITY_EDITOR
        public void UpdateRegisteredGameObjectList()
        {
            RegisteredGameObjects.Clear();

            for (int i = 0; i < eventListeners.Count; i++)
            {
                var go = eventListeners[i].gameObject;
                RegisteredGameObjects.Add(string.Concat(go.scene.name, " - ", go.name));
            }

        }
#endif
	}
}
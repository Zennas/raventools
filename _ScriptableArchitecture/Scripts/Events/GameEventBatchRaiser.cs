﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    public class GameEventBatchRaiser : MonoBehaviour
    {
#if UNITY_EDITOR
        [TextArea(1, 1)]
        public string DeveloperDescription = "";
#endif

        [SerializeField] private GameEvent[] batch;

        public void BatchRaise()
        {
            if (batch == null)
                return;

            for (int i = 0; i < batch.Length; i++)
            {
                if (batch[i] != null)
                    batch[i].Raise();
            }
        }
    }
}
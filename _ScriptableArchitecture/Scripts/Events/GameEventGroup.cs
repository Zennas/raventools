﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Events/GameEventGroup")]
    public class GameEventGroup : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea]
        public string DeveloperDescription = "";
#endif

        [SerializeField] private GameEvent[] group;

        public void GroupRaise(Object context = null)
        {
            if (group == null || group.Length == 0)
                return;

            for (int i = 0; i < group.Length; i++)
            {
                if (group[i] != null)
                    group[i].Raise();
            }
        }
    }
}
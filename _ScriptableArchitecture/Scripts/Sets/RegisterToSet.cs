﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using RoboRyanTron.Unite2017.Events;
using ScriptableArchitecture;

public class RegisterToSet : MonoBehaviour
{
	[SerializeField]
	private GameObjectsRuntimeSet setToJoin;


	// This function is called when the object becomes enabled and active.
	protected void OnEnable()
	{
		if(setToJoin != null)
			setToJoin.Add(gameObject);
	}

	// This function is called when the behaviour becomes disabled () or inactive.
	protected void OnDisable()
	{
		if(setToJoin != null)
			setToJoin.Remove(gameObject);
	}
	
	public void ManualRegister()
	{
		if(setToJoin != null)
			setToJoin.Add(gameObject);
	}
	
	public void ManualUnregister()
	{
		if(setToJoin != null)
			setToJoin.Remove(gameObject); 
	}

}

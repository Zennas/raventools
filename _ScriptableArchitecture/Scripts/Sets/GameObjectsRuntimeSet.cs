﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using RoboRyanTron.Unite2017.Sets;

//namespace RoboRyanTron.Unite2017.Events
namespace ScriptableArchitecture
{
	[CreateAssetMenu(menuName = "ScriptableArchitecture/Sets/GameObjectRuntimeSet")]
	public class GameObjectsRuntimeSet : RuntimeSet<GameObject>
	{
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Controllers/TimeController")]
    public class TimeController : ScriptableObject
    {
        public void SetTimeScale(float scale)
        {
            Time.timeScale = scale;
        }
    }
}
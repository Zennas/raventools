﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Controllers/FadeScreenController")]
    public class FadeScreenController : ScriptableObject
    {
        public float fadeTime;
        public Color fadeColor;
        private Image fader;

        public GameEvent eventToCall;

        public delegate void onFinish();

        public void FadeIn(bool destroyFaderOnComplete = false)
        {
            if (fader == null)
                fader = CreateFadeImage();

            LeanTween.value(fader.gameObject, 0, 1, fadeTime)
                .setOnUpdate((float a) =>
                {
                    Color c = fader.color;
                    c.a = a;
                    fader.color = c;
                })
                .setOnComplete(() =>
                {
                    if (eventToCall != null)
                        eventToCall.Raise();

                    if (destroyFaderOnComplete)
                        Destroy(fader.transform.parent.gameObject);
                });
        }

        public void FadeIn(System.Action action, bool destroyFaderOnComplete = false)
        {
            if (fader == null)
                fader = CreateFadeImage();

            LeanTween.value(fader.gameObject, 0, 1, fadeTime)
                .setOnUpdate((float a) =>
                {
                    Color c = fader.color;
                    c.a = a;
                    fader.color = c;
                })
                .setOnComplete(() =>
                {
                    action();

                    if (destroyFaderOnComplete)
                        Destroy(fader.transform.parent.gameObject);
                });
        }

        public void FadeOut(bool destroyFaderOnComplete = true)
        {
            if (fader == null)
                fader = CreateFadeImage();

            LeanTween.value(fader.gameObject, 1, 0, fadeTime)
                .setOnUpdate((float a) =>
                {
                    Color c = fader.color;
                    c.a = a;
                    fader.color = c;
                })
                .setOnComplete(() =>
                {
                    if (eventToCall != null)
                        eventToCall.Raise();

                    if (destroyFaderOnComplete)
                        Destroy(fader.transform.parent.gameObject);
                });
        }

        public void FadeOut(System.Action action, bool destroyFaderOnComplete = true)
        {
            if (fader == null)
                fader = CreateFadeImage();

            LeanTween.value(fader.gameObject, 1, 0, fadeTime)
                .setOnUpdate((float a) =>
                {
                    Color c = fader.color;
                    c.a = a;
                    fader.color = c;
                })
                .setOnComplete(() =>
                {
                    action();

                    if (destroyFaderOnComplete)
                        Destroy(fader.transform.parent.gameObject);
                });
        }

        private Image CreateFadeImage()//(RectTransform canvas)
        {
            //Create fade Canvas
            var canvasObject = new GameObject("FaderCanvas");
            var canvasComponent = canvasObject.AddComponent<Canvas>();
            canvasComponent.renderMode = RenderMode.ScreenSpaceOverlay;
            canvasComponent.sortingOrder = 9999;

            var canvas = canvasObject.GetComponent<RectTransform>();

            //Create fade Image
            var fade = new GameObject("FaderImage");
            var rt = fade.AddComponent<RectTransform>();
            rt.SetParent(canvas);

            rt.anchorMin = Vector2.zero;
            rt.anchorMax = Vector2.one;
            rt.offsetMin = Vector2.one * -10;
            rt.offsetMax = Vector2.one * 10;

            var img = fade.AddComponent<Image>();
            img.sprite = CreateFadeSprite();
            img.color = fadeColor;
            img.raycastTarget = false;

            return img;
        }

        private Sprite CreateFadeSprite()
        {
            Texture2D tmpTex = new Texture2D(2, 2, TextureFormat.RGBA32, false);

            var pixels = tmpTex.GetPixels();
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = Color.white;
            }
            tmpTex.SetPixels(pixels);
            tmpTex.Apply(false);

            return Sprite.Create(tmpTex, new Rect(0, 0, 2, 2), new Vector2(.5f, .5f));
        }
    }
}
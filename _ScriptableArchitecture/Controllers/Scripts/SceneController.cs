﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

namespace ScriptableArchitecture
{
    [CreateAssetMenu(menuName = "ScriptableArchitecture/Controllers/SceneController")]
    public class SceneController : ScriptableObject
    {
        public void LoadScene(Scene scene)
        {
            SceneManager.LoadScene(scene.buildIndex);
        }

        public void LoadScene(string name)
        {
            SceneManager.LoadScene(name);
        }

        public void LoadScene(int index)
        {
            SceneManager.LoadScene(index);
        }

        public void LoadSceneAsync(string name)
        {
            SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);
        }

        public void LoadSceneAsync(int index)
        {
            SceneManager.LoadSceneAsync(index, LoadSceneMode.Single);
        }

        public void LoadSceneAsyncAdditive(string name)
        {
            if (!SceneManager.GetSceneByName(name).isLoaded)
                SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        }

        public void LoadSceneAsyncAdditive(int index)
        {
            if (!SceneManager.GetSceneByBuildIndex(index).isLoaded)
                SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
        }

        public void SetActiveScene(int sceneIndex)
        {
            var scene = SceneManager.GetSceneAt(sceneIndex);
            SceneManager.SetActiveScene(scene);
        }


        public void ReloadScene()
        {
            LoadScene(SceneManager.GetActiveScene());
        }

        public void UnloadScene(int index)
        {
            SceneManager.UnloadSceneAsync(index);
        }

        public void UnloadScene(string name)
        {
            SceneManager.UnloadSceneAsync(name);
        }


    }
}